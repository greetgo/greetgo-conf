package kz.greetgo.conf.hot;

import kz.greetgo.conf.core.ConfParamType;
import kz.greetgo.conf.env.EnvSource;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.testng.annotations.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class AbstractFileHotConfigFactoryTest {
  private final Random rnd = new Random();

  @RequiredArgsConstructor
  private static class TestFactory extends AbstractFileHotConfigFactory {

    public final Path                configDir;
    public final Map<String, String> env = new HashMap<>();


    @Override
    protected Path configDir() {
      return configDir;
    }

    @Override
    protected @NonNull EnvSource envSource() {
      return env::get;
    }

  }

  public interface TestConfig {
    @Description("str param")
    @DefaultStrValue("def str param value")
    String strParam();

    @Description("int param")
    @DefaultIntValue(45315)
    int intParam();

  }

  private @NonNull Path filesDir() {
    SimpleDateFormat sdf      = new SimpleDateFormat("yyyy-MM-dd-HHmmss-SSS");
    final Path       filesDir = Paths.get("build").resolve(getClass().getSimpleName()).resolve(sdf.format(new Date()) + "-" + rnd.nextInt());

    return filesDir;
  }

  @Test
  @SneakyThrows
  public void createNewTestConfig() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    assertThat(config.strParam()).isEqualTo("def str param value");
    assertThat(config.intParam()).isEqualTo(45315);

    assertThat(configFilePath).exists();

    assertThat(Files.readString(configFilePath)).contains("strParam=def str param value");
    assertThat(Files.readString(configFilePath)).contains("intParam=45315");
  }

  public interface TestConfig2 {


    @Description("str param")
    @DefaultParamType(ConfParamType.ENV)
    @DefaultStrValue("E001")
    String strParam();

    @Description("int param")
    @DefaultParamType(ConfParamType.ENV)
    @DefaultStrValue("E002")
    int intParam();

  }

  @Test
  @SneakyThrows
  public void createNewTestConfigFromEnv() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig2 config         = testFactory.createConfig(TestConfig2.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig2.class);

    testFactory.env.put("E001", "gNbFq7rQx7");
    testFactory.env.put("E002", "23121343");

    assertThat(configFilePath).doesNotExist();

    assertThat(config.strParam()).isEqualTo("gNbFq7rQx7");
    assertThat(config.intParam()).isEqualTo(23121343);

    assertThat(configFilePath).exists();

    assertThat(Files.readString(configFilePath)).contains("strParam:ENV=E001");
    assertThat(Files.readString(configFilePath)).contains("intParam:ENV=E002");
  }

  public interface TestConfig3 {


    @Description("str param")
    @DefaultParamType(ConfParamType.ENV)
    @DefaultStrValue("E001 : 1vWbeVt2gG")
    String strParam();

    @Description("int param")
    @DefaultParamType(ConfParamType.ENV)
    @DefaultStrValue("E002 : 321345325")
    int intParam();

  }

  @Test
  @SneakyThrows
  public void createNewTestConfigFromEnvDefault() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig3 config         = testFactory.createConfig(TestConfig3.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig3.class);

    assertThat(configFilePath).doesNotExist();

    assertThat(config.strParam()).isEqualTo("1vWbeVt2gG");
    assertThat(config.intParam()).isEqualTo(321345325);

    assertThat(configFilePath).exists();

    assertThat(Files.readString(configFilePath)).contains("strParam:ENV=E001 : 1vWbeVt2gG");
    assertThat(Files.readString(configFilePath)).contains("intParam:ENV=E002 : 321345325");
  }

  @Test
  @SneakyThrows
  public void readExistsConfigFile() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    configFilePath.toFile().getParentFile().mkdirs();
    Files.writeString(configFilePath, """
      strParam=WHwN6r0E8z
      intParam=1232124
      """);

    assertThat(config.strParam()).isEqualTo("WHwN6r0E8z");
    assertThat(config.intParam()).isEqualTo(1232124);

  }

  @Test
  @SneakyThrows
  public void readExistsConfigFileFromEnv() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    configFilePath.toFile().getParentFile().mkdirs();
    Files.writeString(configFilePath, """
      strParam : ENV = E001
      intParam : ENV = E002
      """);

    testFactory.env.put("E001", "WHwN6r0E8z");
    testFactory.env.put("E002", "1232124");

    assertThat(config.strParam()).isEqualTo("WHwN6r0E8z");
    assertThat(config.intParam()).isEqualTo(1232124);

  }

  @Test
  @SneakyThrows
  public void readExistsConfigFileFromEnvWithDefault() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    configFilePath.toFile().getParentFile().mkdirs();
    Files.writeString(configFilePath, """
      strParam : ENV = E001 : left
      intParam : ENV = E002 : 111
      """);

    testFactory.env.put("E001", "WHwN6r0E8z");
    testFactory.env.put("E002", "1232124");

    assertThat(config.strParam()).isEqualTo("WHwN6r0E8z");
    assertThat(config.intParam()).isEqualTo(1232124);
  }

  @Test
  @SneakyThrows
  public void readExistsConfigFileFromEnvDefault() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    configFilePath.toFile().getParentFile().mkdirs();
    Files.writeString(configFilePath, """
      strParam : ENV = E001 : WHwN6r0E8z
      intParam : ENV = E002 : 1232124
      """);

    assertThat(config.strParam()).isEqualTo("WHwN6r0E8z");
    assertThat(config.intParam()).isEqualTo(1232124);

  }

  @Test
  @SneakyThrows
  public void readExistsConfigFileFromEnv_appendAbsentParameter() {
    final Path        filesDir       = filesDir();
    final TestFactory testFactory    = new TestFactory(filesDir);
    final TestConfig  config         = testFactory.createConfig(TestConfig.class);
    final Path        configFilePath = testFactory.getConfigFilePath(TestConfig.class);

    assertThat(configFilePath).doesNotExist();

    configFilePath.toFile().getParentFile().mkdirs();
    Files.writeString(configFilePath, """
      strParam : ENV = E001 : left
      """);

    testFactory.env.put("E001", "WHwN6r0E8z");
    testFactory.env.put("E002", "1232124");

    assertThat(config.strParam()).isEqualTo("WHwN6r0E8z");
    assertThat(config.intParam()).isEqualTo(45315);

    final String configText = Files.readString(configFilePath);

    assertThat(configText).contains("intParam=45315");
  }
}