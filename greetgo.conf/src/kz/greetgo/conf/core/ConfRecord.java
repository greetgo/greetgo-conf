package kz.greetgo.conf.core;

import kz.greetgo.conf.hot.DefaultParamType;
import kz.greetgo.conf.hot.Description;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.util.Arrays.asList;

/**
 * Запись конфига
 */
@EqualsAndHashCode
public class ConfRecord {
  /**
   * Комментарии перед записью
   */
  public final List<String> comments = new ArrayList<>();

  /**
   * Ключ, которому присваивается значение
   * <p>
   * Если null, то данная запись содержит только комментарий
   */
  private String key;

  /**
   * Тип записи. Определяет, каким образом получается конечное значение
   * <p>
   * DIRECT - непосредственно из поля value
   * <p>
   * ENV - поле value содержит имя переменной окружения, из которого читается конечное значение
   */
  private ConfParamType type;

  /**
   * Присваиваемое значение
   * <p>
   * Должен быть null, если <code>{@link #key} == null</code>
   */
  private String value;

  public String trimmedKey() {
    String x = key;
    if (x == null) return null;
    String xx = x.trim();
    return xx.isEmpty() ? null : xx;
  }

  public String trimmedValue() {
    String x = value;
    return x == null ? null : x.trim();
  }

  public String key() {
    return key;
  }

  public String value() {
    return value;
  }

  public ConfParamType type() {
    return type;
  }

  @Override
  public String toString() {
    List<String> ss = new ArrayList<>();

    if (key != null) {
      ss.add(value == null ? '`' + key + '`' : '`' + key + '`' + '=' + '`' + value + '`');
    }

    String oneLineComment = oneLineComment();
    if (oneLineComment != null) {
      ss.add("// " + oneLineComment);
    }
    return getClass().getSimpleName() + '{' + String.join(" ", ss) + '}';
  }

  private String oneLineComment() {
    if (comments.isEmpty()) {
      return null;
    }
    String comment = String.join("\n", comments);
    //noinspection RedundantEscapeInRegexReplacement
    return comment.replaceAll("\n", "\\n");
  }

  public static @NonNull ConfRecord ofComment(String comment) {
    List<String> comments = comment == null ? Collections.emptyList() : asList(comment.split("\n"));
    return ofComments(comments);
  }

  public static @NonNull ConfRecord of(String key, String value, List<String> comments) {
    return of(key, ConfParamType.VALUE, value, comments);
  }

  public static @NonNull ConfRecord of(String key, ConfParamType type, String value, List<String> comments) {
    ConfRecord ret = new ConfRecord();
    ret.comments.addAll(comments);
    ret.key   = key;
    ret.type  = type;
    ret.value = value;
    return ret;
  }

  public static @NonNull ConfRecord ofDescription(String key, String value, Description description, DefaultParamType defaultParamType) {
    return of(key, typeFrom(defaultParamType), value, description == null ? Collections.emptyList() : asList(description.value().split("\n")));
  }

  private static ConfParamType typeFrom(DefaultParamType defaultParamType) {
    return defaultParamType == null ? ConfParamType.VALUE : defaultParamType.value();
  }

  public static @NonNull ConfRecord of(String key, String value, String comment) {
    List<String> comments = comment == null ? Collections.emptyList() : asList(comment.split("\n"));
    return of(key, value, comments);
  }

  public static @NonNull ConfRecord of(String key, String value) {
    return of(key, value, Collections.emptyList());
  }

  public static @NonNull ConfRecord of(String key, ConfParamType type, String value) {
    return of(key, type, value, Collections.emptyList());
  }

  public static @NonNull ConfRecord ofComments(List<String> comments) {
    ConfRecord ret = new ConfRecord();
    ret.comments.addAll(comments);
    ret.key   = null;
    ret.type  = ConfParamType.VALUE;
    ret.value = null;
    return ret;
  }

  public void appendTo(List<String> lines) {
    for (String comment : comments) {
      if (comment == null || comment.isEmpty()) {
        lines.add("#");
      } else {
        lines.add("# " + comment);
      }
    }
    if (value != null) {
      Objects.requireNonNull(key);
      lines.add(keyWithType() + '=' + value);
    } else if (key != null) {
      lines.add(keyWithType());
    }
  }

  private String keyWithType() {
    final String        key  = this.key;
    final ConfParamType type = this.type;
    if (type == null || type == ConfParamType.VALUE) {
      return key;
    }
    return key + ":" + type;
  }

  public void insertTopComment(String comment) {
    List<String> addingComments      = ConfRecord.ofComment(comment).comments;
    int          addingCommentsCount = addingComments.size();
    for (int i = 0; i < addingCommentsCount; i++) {
      comments.add(0, null);
    }
    for (int i = 0; i < addingCommentsCount; i++) {
      comments.set(i, addingComments.get(i));
    }
  }

  public String commentValue() {
    if (comments.isEmpty()) return null;
    return String.join("\n", comments);
  }
}
