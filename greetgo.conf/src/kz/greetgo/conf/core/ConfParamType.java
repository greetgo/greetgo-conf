package kz.greetgo.conf.core;

public enum ConfParamType {
  VALUE,
  ENV,
  ;

  public static ConfParamType parseOrElse(String string, ConfParamType defaultValue) {
    if (string == null) return defaultValue;
    String upperCase = string.toUpperCase();
    for (ConfParamType value : values()) {
      if (value.name().equals(upperCase)) {
        return value;
      }
    }
    return defaultValue;
  }
}
