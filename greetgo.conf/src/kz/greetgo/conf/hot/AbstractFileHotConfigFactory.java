package kz.greetgo.conf.hot;

import kz.greetgo.conf.core.ConfAccess;
import kz.greetgo.conf.core.ConfAccessFile;
import kz.greetgo.conf.core.ConfContentSerializer;

import java.nio.file.Path;

public abstract class AbstractFileHotConfigFactory extends AbstractHotConfFactory {
  @Override
  protected <T> ConfAccess confAccess(Class<T> configInterface) {
    final Path                  filePath              = getConfigFilePath(configInterface);
    final ConfContentSerializer confContentSerializer = confContentSerializer();
    final ConfAccessFile        confAccessFile        = new ConfAccessFile(filePath.toFile(), confContentSerializer);

    return confAccessFile;
  }

  public <T> Path getConfigFilePath(Class<T> configInterface) {
    final String interfaceName = extractInterfaceName(configInterface);
    final String fileName      = interfaceName + hotConfigExtension();
    final Path   filePath      = configDir().resolve(fileName);

    return filePath;
  }

  protected abstract Path configDir();

  protected String hotConfigExtension() {
    return ".hotconfig";
  }


}
