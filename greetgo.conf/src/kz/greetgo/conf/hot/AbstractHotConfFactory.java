package kz.greetgo.conf.hot;

import kz.greetgo.conf.core.ConfAccess;
import kz.greetgo.conf.core.ConfAccessStdSerializer;
import kz.greetgo.conf.core.ConfContentSerializer;
import kz.greetgo.conf.core.ConfImplBuilder;
import kz.greetgo.conf.env.EnvSource;
import lombok.NonNull;

import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractHotConfFactory {
  private final ConcurrentHashMap<Class<?>, Object> proxyMap = new ConcurrentHashMap<>();

  public <T> @NonNull T createConfig(Class<T> configInterface) {
    {
      //noinspection unchecked
      T ret = (T) proxyMap.get(configInterface);
      if (ret != null) return ret;
    }

    synchronized (proxyMap) {
      {
        //noinspection unchecked
        T ret = (T) proxyMap.get(configInterface);
        if (ret != null) return ret;
      }

      {
        T ret = buildConfigProxy(configInterface);
        proxyMap.put(configInterface, ret);
        return ret;
      }
    }
  }

  protected long currentTimeMillis() {
    return System.currentTimeMillis();
  }

  private <T> @NonNull T buildConfigProxy(Class<T> configInterface) {
    return ConfImplBuilder.confImplBuilder(configInterface, confAccess(configInterface))
                          .changeCheckTimeoutMs(this::autoResetTimeout)
                          .currentTimeMillis(this::currentTimeMillis)
                          .envAccess(s -> envSource().getValue(s))
                          .build();
  }

  protected @NonNull EnvSource envSource() {
    return EnvSource.SYSTEM;
  }

  /**
   * Defines auto reset timeout. It is a time interval in milliseconds to check last config modification date and time.
   * And if the date and time was changed, then it calls method `reset` for this config.
   *
   * @return auto reset timeout. Zero - auto resetting is off
   */
  protected long autoResetTimeout() {
    return 10_000;
  }

  protected abstract <T> ConfAccess confAccess(Class<T> configInterface);

  public void reset() {
    proxyMap.clear();
  }

  protected @NonNull ConfContentSerializer confContentSerializer() {
    return new ConfAccessStdSerializer();
  }

  public @NonNull String extractInterfaceName(@NonNull Class<?> configInterface) {
    ConfigFileName x = configInterface.getAnnotation(ConfigFileName.class);
    return x != null ? x.value() : configInterface.getSimpleName();
  }

}
