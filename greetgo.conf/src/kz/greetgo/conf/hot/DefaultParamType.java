package kz.greetgo.conf.hot;

import kz.greetgo.conf.core.ConfParamType;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

/**
 * Perform default parameter type
 *
 * @author pompei
 */
@Documented
@Target({METHOD, FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DefaultParamType {

  /**
   * Perform default parameter type
   *
   * @return default parameter type
   */
  ConfParamType value();

}
