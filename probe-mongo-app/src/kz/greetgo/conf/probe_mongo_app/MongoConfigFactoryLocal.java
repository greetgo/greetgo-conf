package kz.greetgo.conf.probe_mongo_app;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import kz.greetgo.conf.mongo.factory.AbstractMongoConfigFactory;
import kz.greetgo.conf.mongo.model.ConfigMongoStoreDto;
import kz.greetgo.conf.mongo_test_connection.MongoTestConnections;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.IntAccessor;
import lombok.NonNull;

public class MongoConfigFactoryLocal extends AbstractMongoConfigFactory {

  private final String      rootPath;
  private final IntAccessor autoResetTimeout;

  public MongoConfigFactoryLocal(String rootPath, @NonNull FUI fui) {
    this.rootPath    = rootPath;
    autoResetTimeout = fui.entryInt(rootPath + "/autoResetTimeout", 3000);
  }

  @Override
  protected long autoResetTimeout() {
    return autoResetTimeout.getInt();
  }

  @Override
  protected MongoCollection<ConfigMongoStoreDto> table() {
    final MongoClient mongoClient = MongoTestConnections.mongoClient();

    final MongoDatabase database = mongoClient.getDatabase("probe_mongo_app");

    return database.getCollection("config", ConfigMongoStoreDto.class);
  }

  @Override
  protected String rootPath() {
    return rootPath;
  }

}
