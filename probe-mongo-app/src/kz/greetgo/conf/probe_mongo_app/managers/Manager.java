package kz.greetgo.conf.probe_mongo_app.managers;

import kz.greetgo.util.fui.BoolAccessor;
import kz.greetgo.util.fui.FUI;
import kz.greetgo.util.fui.handler.BoolChangeHandler;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public abstract class Manager {

  protected final String group;

  protected String menuRoot() {
    return group + "/" + getClass().getSimpleName();
  }

  protected Manager(String group) {
    this.group = group;
  }

  protected abstract void idle();

  @SneakyThrows
  public void install(@NonNull FUI fui) {

    final BoolChangeHandler changeHandler = new BoolChangeHandler() {

      private final AtomicReference<Thread> thread = new AtomicReference<>(null);

      @Override
      public void changed(boolean boolValue) {
        if (!boolValue) {
          thread.set(null);
          log.info("31jEPXkNQg :: Deactivate idle thread");
          return;
        }

        if (thread.get() != null) {
          return;
        }

        final Thread t = new Thread(this::idleRun);
        thread.set(t);
        t.start();
        log.info("KPq1n0T1rv :: Activate idle thread");
      }

      void idleRun() {
        log.info("31bpV9zh5p :: Idle thread started: " + Thread.currentThread().getName());

        while (thread.get() != null && fui.isAlive()) {

          idle();

          try {
            Thread.sleep(1000);
          } catch (InterruptedException ignore) {
            break;
          }

        }

        log.info("2rVh0TMr6q :: Idle thread finished: " + Thread.currentThread().getName());

      }

    };

    final BoolAccessor boolAccessor = fui.entryBool(menuRoot() + "/idle", false, changeHandler);

    changeHandler.changed(boolAccessor.is());
  }
}
