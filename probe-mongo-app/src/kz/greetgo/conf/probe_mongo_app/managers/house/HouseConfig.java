package kz.greetgo.conf.probe_mongo_app.managers.house;

import kz.greetgo.conf.hot.DefaultIntValue;
import kz.greetgo.conf.hot.DefaultLongValue;
import kz.greetgo.conf.hot.Description;

@Description("Бра-аво-с")
public interface HouseConfig {

  @Description("Connect timeout in ms")
  @DefaultLongValue(134_000)
  long connectTimeoutMs();

  @Description("Size of batch size")
  @DefaultIntValue(1000)
  int batchSize();

}
