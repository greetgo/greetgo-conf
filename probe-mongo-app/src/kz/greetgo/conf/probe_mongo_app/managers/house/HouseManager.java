package kz.greetgo.conf.probe_mongo_app.managers.house;

import kz.greetgo.conf.probe_mongo_app.managers.Manager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HouseManager extends Manager {

  private final HouseConfig conf;

  public HouseManager(String group, HouseConfig conf) {
    super(group);
    this.conf = conf;
  }

  @Override
  protected void idle() {
    if (log.isInfoEnabled()) {
      log.info("E07R1AT6Oh :: [" + getClass().getSimpleName() + "]"
                 + "  connectTimeoutMs = " + conf.connectTimeoutMs()
                 + ", batchSize = " + conf.batchSize());
    }
  }

}
