package kz.greetgo.conf.probe_mongo_app.managers.star;

import kz.greetgo.conf.probe_mongo_app.managers.Manager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StarManager extends Manager {
  private final StarConfig starConfig;

  public StarManager(String group, StarConfig starConfig) {
    super(group);
    this.starConfig = starConfig;
  }

  private String connectStr() {
    return "https://"
             + starConfig.username() + ":" + starConfig.password() + "@"
             + starConfig.host() + ":" + starConfig.port()
             + starConfig.options();
  }

  @Override
  protected void idle() {
    log.info("958AGl25uv :: " + getClass().getSimpleName() + " [" + group + "] Connect str " + connectStr());

  }
}
