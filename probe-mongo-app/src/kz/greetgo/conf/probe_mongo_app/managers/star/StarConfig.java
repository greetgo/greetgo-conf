package kz.greetgo.conf.probe_mongo_app.managers.star;

import kz.greetgo.conf.hot.DefaultIntValue;
import kz.greetgo.conf.hot.DefaultStrValue;
import kz.greetgo.conf.hot.Description;

@Description("Star config WOW\nHello World\nMain Status")
public interface StarConfig {

  @DefaultStrValue("192.168.23.34")
  String host();

  @DefaultIntValue(34540)
  int port();

  @DefaultStrValue("Don-Stone")
  String username();

  @DefaultStrValue("111-111")
  String password();

  @DefaultStrValue("/loan/minus?sinus=317&follow=StatusOfClose")
  String options();

}
