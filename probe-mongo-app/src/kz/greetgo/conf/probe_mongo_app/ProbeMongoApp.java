package kz.greetgo.conf.probe_mongo_app;

import kz.greetgo.conf.probe_mongo_app.managers.Manager;
import kz.greetgo.conf.probe_mongo_app.managers.house.HouseConfig;
import kz.greetgo.conf.probe_mongo_app.managers.house.HouseManager;
import kz.greetgo.conf.probe_mongo_app.managers.star.StarConfig;
import kz.greetgo.conf.probe_mongo_app.managers.star.StarManager;
import kz.greetgo.util.fui.FUI;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ProbeMongoApp {
  public static void main(String[] args) {
    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();
    new ProbeMongoApp().execute();
  }

  private void execute() {
    log.info("Dv2bOj62Gi :: Application Starting...");

    final Path                    appRoot     = Paths.get("build").resolve("mongo-app");
    final FUI                     fui         = new FUI(appRoot);
    final MongoConfigFactoryLocal factoryOne  = new MongoConfigFactoryLocal("one", fui);
    final MongoConfigFactoryLocal factoryTwo  = new MongoConfigFactoryLocal("two", fui);
    final List<Manager>           managerList = new ArrayList<>();

    managerList.add(new StarManager("one", factoryOne.createConfig(StarConfig.class)));
    managerList.add(new HouseManager("one", factoryOne.createConfig(HouseConfig.class)));
    managerList.add(new StarManager("two", factoryTwo.createConfig(StarConfig.class)));
    managerList.add(new HouseManager("two", factoryTwo.createConfig(HouseConfig.class)));

    managerList.forEach(m -> m.install(fui));

    log.info("Dv2bOj62Gi :: Application Started");
    fui.go();
    log.info("Dv2bOj62Gi :: Application Finished");
  }
}
