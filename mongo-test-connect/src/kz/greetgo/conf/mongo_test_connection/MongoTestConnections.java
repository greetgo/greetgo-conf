package kz.greetgo.conf.mongo_test_connection;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import lombok.NonNull;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.requireNonNullElseGet;

public class MongoTestConnections {

  private static final AtomicReference<MongoClient> mongoClient = new AtomicReference<>(null);

  public static MongoClient mongoClient() {
    return mongoClient.updateAndGet(
      mongoClient -> requireNonNullElseGet(mongoClient, MongoTestConnections::createMongoClient));
  }

  private static @NonNull MongoClient createMongoClient() {
    PojoCodecProvider.Builder pojoBuilder = PojoCodecProvider.builder();
    pojoBuilder.automatic(true);

    final CodecRegistry defaultCodecRegistry = MongoClientSettings.getDefaultCodecRegistry();

    final CodecRegistry codecRegistry = CodecRegistries.fromProviders(defaultCodecRegistry, pojoBuilder.build());

    ConnectionString connectionString = new ConnectionString("mongodb://localhost:51080");

    final MongoClientSettings settings = MongoClientSettings.builder()
                                                            .codecRegistry(codecRegistry)
                                                            .applyConnectionString(connectionString)
                                                            .build();

    return MongoClients.create(settings);
  }

}
