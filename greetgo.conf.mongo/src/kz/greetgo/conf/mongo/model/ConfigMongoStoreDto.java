package kz.greetgo.conf.mongo.model;

import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.util.Date;

@ToString
@FieldNameConstants
public class ConfigMongoStoreDto {

  public String id;

  public String text;

  public Date lastModifiedAt;

}
