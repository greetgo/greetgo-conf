package kz.greetgo.conf.mongo.factory;

import com.mongodb.client.MongoCollection;
import kz.greetgo.conf.core.ConfAccess;
import kz.greetgo.conf.hot.AbstractHotConfFactory;
import kz.greetgo.conf.mongo.access.MongoConfAccess;
import kz.greetgo.conf.mongo.model.ConfigMongoStoreDto;

import java.util.Date;

public abstract class AbstractMongoConfigFactory extends AbstractHotConfFactory {

  protected abstract MongoCollection<ConfigMongoStoreDto> table();

  protected abstract String rootPath();

  protected String generatePath(String interfaceName) {
    return rootPath() + "/" + interfaceName;
  }

  @Override
  protected <T> ConfAccess confAccess(Class<T> configInterface) {
    return new MongoConfAccess(this::table,
                               this::confContentSerializer,
                               () -> generatePath(extractInterfaceName(configInterface)),
                               () -> new Date(currentTimeMillis()));
  }

}
