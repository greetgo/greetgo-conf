package kz.greetgo.conf.mongo.access;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import kz.greetgo.conf.core.ConfAccess;
import kz.greetgo.conf.core.ConfContent;
import kz.greetgo.conf.core.ConfContentSerializer;
import kz.greetgo.conf.mongo.model.ConfigMongoStoreDto;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.function.Supplier;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

@Slf4j
public class MongoConfAccess implements ConfAccess {
  private final Supplier<String>                               path;
  private final Supplier<MongoCollection<ConfigMongoStoreDto>> table;
  private final Supplier<ConfContentSerializer>                confContentSerializer;
  private final Supplier<Date>                                 now;

  public MongoConfAccess(Supplier<MongoCollection<ConfigMongoStoreDto>> table,
                         Supplier<ConfContentSerializer> confContentSerializer,
                         Supplier<String> path,
                         Supplier<Date> now) {

    this.table                 = table;
    this.confContentSerializer = confContentSerializer;
    this.path                  = path;
    this.now                   = now;
  }

  private String id() {
    return path.get();
  }

  private MongoCollection<ConfigMongoStoreDto> table() {
    return this.table.get();
  }

  @Override
  public void write(ConfContent confContent) {
    final String text = confContent == null ? null : confContentSerializer.get().serialize(confContent);

    if (text == null) {
      table().deleteOne(eq("_id", id()));
    } else {
      table().updateOne(eq("_id", id()),
                        combine(
                          set(ConfigMongoStoreDto.Fields.text, text),
                          set(ConfigMongoStoreDto.Fields.lastModifiedAt, now.get())
                        ),
                        new UpdateOptions().upsert(true));
    }

  }

  @Override
  public Date lastModifiedAt() {
    long start = System.currentTimeMillis();

    try {
      final Date storedDate = table().find(eq("_id", id()))
                                     .projection(include(ConfigMongoStoreDto.Fields.lastModifiedAt))
                                     .map(x -> x.lastModifiedAt)
                                     .first();

      if (storedDate != null) {
        return storedDate;
      }

      final Date now = this.now.get();

      table().updateOne(eq("_id", id()), set(ConfigMongoStoreDto.Fields.lastModifiedAt, now));

      return now;
    } finally {
      if (log.isDebugEnabled()) {
        log.debug("6L668IIYiN :: Called lastModifiedAt over " + path.get()
                    + " for " + (System.currentTimeMillis() - start) + " ms");
      }
    }
  }

  @Override
  public ConfContent load() {

    final String text = table().find(eq("_id", id()))
                               .map(x -> x.text)
                               .first();

    return confContentSerializer.get().deserialize(text);
  }
}
