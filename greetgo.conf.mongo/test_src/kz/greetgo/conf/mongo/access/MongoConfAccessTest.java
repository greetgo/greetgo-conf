package kz.greetgo.conf.mongo.access;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import kz.greetgo.conf.core.ConfAccessStdSerializer;
import kz.greetgo.conf.core.ConfContent;
import kz.greetgo.conf.core.ConfRecord;
import kz.greetgo.conf.mongo.model.ConfigMongoStoreDto;
import kz.greetgo.conf.mongo_test_connection.MongoTestConnections;
import kz.greetgo.util.RND;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.assertThat;

public class MongoConfAccessTest {

  protected MongoCollection<ConfigMongoStoreDto> table;

  @BeforeMethod
  public void prepareCollection(@NonNull Method method) {
    final MongoClient   mongoClient = MongoTestConnections.mongoClient();
    final MongoDatabase database    = mongoClient.getDatabase(getClass().getSimpleName());
    table = database.getCollection(method.getName() + "_" + RND.strEng(5), ConfigMongoStoreDto.class);
  }

  @Test
  public void write__probe() {
    final MongoConfAccess mongoConfAccess = new MongoConfAccess(
      () -> table, ConfAccessStdSerializer::new, () -> "test/sub_dir", Date::new
    );

    ConfContent confContent = new ConfContent();

    confContent.records.add(ConfRecord.of("sigma.status", "Hello World", "Просто сигма"));
    confContent.records.add(ConfRecord.of("sigma.orion", "Парашют стреляет вправо", "О парашюте"));
    confContent.records.add(ConfRecord.of("neon.color", "RED", "Про цвет"));

    //
    //
    mongoConfAccess.write(confContent);
    //
    //

  }

  @Test
  @SneakyThrows
  public void write__read__lastModifiedAt() {
    SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Calendar         time = new GregorianCalendar();

    time.setTime(sdf.parse("2023-11-18 17:01:32"));

    final MongoConfAccess mongoConfAccess = new MongoConfAccess(
      () -> table, ConfAccessStdSerializer::new, () -> "test/sub_dir/deep_dir/status", time::getTime
    );

    //
    //
    final Date lastModifiedAt1 = mongoConfAccess.lastModifiedAt();
    //
    //

    assertThat(lastModifiedAt1).isNull();

    //
    //
    final ConfContent content1 = mongoConfAccess.load();
    //
    //

    assertThat(content1).isEqualTo(ConfContent.empty());

    ConfContent content2 = new ConfContent();

    content2.records.add(ConfRecord.of("sigma.status", "Hello World", "Просто сигма"));
    content2.records.add(ConfRecord.of("sigma.orion", "Парашют стреляет вправо", "О парашюте"));
    content2.records.add(ConfRecord.of("neon.color", "RED", "Про цвет"));

    //
    //
    mongoConfAccess.write(content2);
    //
    //

    //
    //
    final Date lastModifiedAt2 = mongoConfAccess.lastModifiedAt();
    //
    //

    assertThat(sdf.format(lastModifiedAt2)).isEqualTo("2023-11-18 17:01:32");

    //
    //
    final ConfContent content3 = mongoConfAccess.load();
    //
    //

    assertThat(content3).isEqualTo(content2);

    content2.records.add(ConfRecord.of("forest.color", "GREEN", "Про цвет"));

    time.setTime(sdf.parse("2023-11-19 11:11:11"));

    //
    //
    mongoConfAccess.write(content2);
    //
    //


    //
    //
    final ConfContent content4 = mongoConfAccess.load();
    //
    //

    assertThat(content4).isEqualTo(content2);

    //
    //
    final Date lastModifiedAt3 = mongoConfAccess.lastModifiedAt();
    //
    //

    assertThat(sdf.format(lastModifiedAt3)).isEqualTo("2023-11-19 11:11:11");

    time.setTime(sdf.parse("2023-11-19 13:13:13"));

    //
    //
    mongoConfAccess.write(null);
    //
    //

    //
    //
    final Date lastModifiedAt4 = mongoConfAccess.lastModifiedAt();
    //
    //

    assertThat(lastModifiedAt4).isNull();

    //
    //
    final ConfContent content5 = mongoConfAccess.load();
    //
    //

    assertThat(content5).isEqualTo(ConfContent.empty());
  }

}
